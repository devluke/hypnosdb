const _ = require("underscore");
const fs = require("fs");

var database = [];

var filename = "./hypnos.json";
var inMemoryOnly = false;
var timestampData = false;
var autoload = true;
var autosave = true;

var self;

/**
 * Create a new collection
 * @constructor
 * @param {String} options.filename Optional, database will be in-memory only if not specified
 * @param {Boolean} options.inMemoryOnly Optional, defaults to false
 * @param {Boolean} options.timestampData Optional, defaults to false. If set to true, createdAt and updatedAt will be created and populated automatically (if not specified by user)
 * @param {Boolean} options.autoload Optional, defaults to true
 * @param {Boolean} options.autosave Optional, defaults to true
*/
function Hypnos(options) {

	// Set inMemoryOnly to true if no options are set
	if (!options) {
		
		inMemoryOnly = true;

		return;
	
	}

	// If options is a string, set filename to options
	if (typeof options === "string") { filename = options; }

	// Set options
	else {

		filename = options.filename || filename;
		inMemoryOnly = options.inMemoryOnly || inMemoryOnly;
		timestampData = options.timestampData || timestampData;
		autoload = options.autoload || autoload;
		autosave = options.autosave || autosave;

	}

	// Load database if autoload is true
	if (autoload) { this.loadDatabase(); }

	// Initialize self variable
	self = this;

}

/**
 * Load the database
 * @param {Function} cb Optional callback, signature: (err)
 */
Hypnos.prototype.loadDatabase = (cb) => {

	const callback = cb || (() => {});

	if (inMemoryOnly) { return callback(new Error("Database is in-memory only")); }

	fs.readFile(filename, (err, data) => {

		if (err) { return callback(err); }

		database = JSON.parse(data);

	});

	console.log(database);

};

/**
 * Save the database
 * @param {Function} cb Optional callback, signature: (err)
 */
Hypnos.prototype.saveDatabase = (cb) => {

	const callback = cb || (() => {});

	if (inMemoryOnly) { return callback(new Error("Database is in-memory only")); }

	const string = JSON.stringify(database, null, 2);

	fs.writeFile(filename, string, (err) => {

		if (err) { return callback(err); }

	});

	return callback(null);

};

/**
 * Find a document
 * @param {Object} query
 * @param {Function} cb Optional callback, signature: (err, docs)
 */
Hypnos.prototype.find = (query, cb) => {

	const callback = cb || (() => {});

	if (autoload) { self.loadDatabase(); }

	const docs = _.where(database, query);

	return callback(null, docs);

};

/**
 * Insert a document
 * @param {Object} document
 * @param {Function} cb Optional callback, signature: (err, insertedDoc)
 */
Hypnos.prototype.insert = (document, cb) => {

	const callback = cb || (() => {});

	if (!(typeof document === "object")) { return callback(new Error("Document must be an object")); }
	if (document.size < 1) { return callback(new Error("Document must have a size of 1 or more")); }

	var newDocument = document;

	if (timestampData) {

		const now = Date();

		if (newDocument.createdAt === undefined) { newDocument.createdAt = now; }
		if (newDocument.updatedAt === undefined) { newDocument.updatedAt = now; }

	}

	newDocument._id = (database.size == 0) ? (_.last(database)._id + 1) : 1;

	database.push(newDocument);

	if (autosave) { self.saveDatabase(); }

	return callback(null, newDocument);

};

/**
 * Remove a document
 * @param {Object} query
 * @param {Function} cb Optional callback, signature: (err, numRemoved)
 */
Hypnos.prototype.remove = (query, cb) => {

	const callback = cb || (() => {});

	if (!(typeof query === "object")) { return callback(new Error("Query must be an object")); }
	if (query.size != 1) { return callback(new Error("Query must have a size of 1")); }

	const docs = _.where(database, query);

	var numRemoved = 0;

	docs.forEach(doc => {

		const index = database.indexOf(doc);

		database.splice(index, 1);

		numRemoved++;

	});

	if (autosave) { self.saveDatabase(); }

	return callback(null, numRemoved);

};

/**
 * Update a document
 * @param {Object} query
 * @param {Object} update
 * @param {Function} cb Optional callback, signature: (err, numAffected, affectedDocs)
 */
Hypnos.prototype.update = (query, update, cb) => {

	const callback = cb || (() => {});

	if (!(typeof query === "object")) { return callback(new Error("Query must be an object")); }
	if (query.size != 1) { return callback(new Error("Query must have a size of 1")); }

	if (!(typeof update === "object")) { return callback(new Error("Update must be an object")); }
	if (update.size < 1) { return callback(new Error("Update must have a size of 1 or more")); }

	if (Array.from(update.keys).contains("_id")) { return callback(new Error("You cannot update _id")); }

	const docs = _.where(database, query);

	var numAffected = 0;

	var affectedDocs = [];

	docs.forEach(doc => {

		var newDocument = doc;
		
		update.forEach((value, key) => {

			newDocument[key] = value;

		});

		if (timestampData) {

			newDocument.updatedAt = Date();

		}

		const index = database.indexOf(doc);

		database.splice(index, 1, newDocument);

		numAffected++;

		affectedDocs.push(newDocument);

	});

	if (autosave) { self.saveDatabase(); }

	return callback(null, numAffected, affectedDocs);

};

module.exports = Hypnos;